#!/bin/bash

# Let's parse first the cmd args
if [[ "$#" -ne 2 ]] ; then
    echo "Invalid number of params passed!"
    exit 1
fi

src="$1"
dst="$2"

echo "Source: $src, Destination: $dst"
# Got it, let's move on

# Let's get the images from src
shopt -s globstar       # Gonna need this for recursive globbing mhmmm
for img in "$src"/**
do
    echo "$img"
    if [[ ! -f "$img" ]]
    then
        continue
    fi

    time=$(stat --printf="%y" "$img")
    echo "$time"
    # Let's cut the year and month!
    [[ "$time" =~ ([[:digit:]]{4})\-([[:digit:]]{2}).* ]]
    year="${BASH_REMATCH[1]}"
    month="${BASH_REMATCH[2]}"
    folderName="$year-$month"
    echo "$folderName"

    # Let's check if it exists
    if [[ ! -e "$dst"/"$folderName" ]]
    then
        #create first!
        mkdir -p ./"$dst"/"$folderName"
    fi

    # Let's move it there
    mv "$img" ./"$dst"/"$folderName"
done
