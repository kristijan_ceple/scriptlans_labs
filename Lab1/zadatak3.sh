#!/bin/bash

for file in localhost_access_log.*.txt
do
    #echo "$file"

    # Now we have a single file opened - let's capture the date first
    [[ "$file" =~  [[:digit:]]{4}\-[[:digit:]]{2}\-[[:digit:]]{2} ]]
    date="$BASH_REMATCH"

    # Start teh print
    echo
    echo "date: $date"
    echo "-------------------------------------------------------"

    declare -A requestsDict     # Declare the dictionary
    while read row
    do
        # Have got the row now -- cut the interesting part now
        # It's located between quotes hehehh easy
        [[ "$row" =~ \".*\" ]]
        importantPart="${BASH_REMATCH:1:-1}"
        # echo "$importantPart"

        ((requestsDict[$importantPart]++))
    done < "$file"

    # Print the dictionary for today's log file!
    # Need to store it all into one var, then sort the var and finally print it!
    toSort=""
    for entry in "${!requestsDict[@]}"
    do
        printf -v result "\t%d: %s\n" "${requestsDict[$entry]}" "$entry"
        toSort+="$result"
        #printf "%s" "$result"
    done

    printf "%s" "$toSort" | sort -nrk 1

    # Now reset the associative array
    unset requestsDict
done