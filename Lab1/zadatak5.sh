#!/bin/bash

if [[ "$#" -ne 2 ]]
then
    echo "Invalid number of parameters passed! Expected 2: source dir and format"
    exit -1
fi

echo "Passed cmd arguments: $@"

src="$1"
format="$2"
echo "Source: $src, Format: $format"

shopt -s globstar
linesTotal=0
for dir in ./"$src"/**
do
    #echo "Current dir: $dir"

    if [[ ! -d "$dir" ]]
    then
        continue
    fi

    #printf "For iteration directory: %s\n" "$dir"

    # Now do the format!
    evalFormat=$(eval echo "$dir"/"$format")
    #echo "Eval-led format: $evalFormat"

    for match in $evalFormat
    do
        #echo "Check if $match is a file!"
        # Check if file
        if [[ ! -f "$match" ]]
        then
            continue
        fi

        #echo "Currently counting lines in file: $match"

        # Now let's do this!
        output=$(wc -l "$match")
        numba=$(echo "$output" | cut -d " " -f 1)
        echo "Lines in file $match = $numba"

        (( linesTotal+=numba ))
    done

done

printf "Total lines in scanned files: %d\n" "$linesTotal"