#!/bin/bash

if [[ $# -ne 2 ]]
then
    echo "Invalid number of parameters passed! Expected 2: source dir and format"
fi

src=$1
format=$2
echo "Source: $src, Format: $format"

shopt -s globstar
for dir in "$src"/**
do
    if [[ ! -d "$dir" ]]
    then
        continue
    fi

    printf "For iteration: %s\n" "$dir"

    # Now do the format!
    for match in "$dir"/$format
    do
        # Check if file
        if [[ ! -f "$match" ]]
        then
            continue
        fi
    done 
done