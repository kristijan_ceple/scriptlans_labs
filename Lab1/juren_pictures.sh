#!/bin/bash

for i in {1..500}
do
	year=$(($i*$i*47%10+2010))
	day=$(($i*$i*47%15+10))
	month=$(($i%9+1))

touch -m -a -t ${year}0${month}${day}0130.09 ./picturinhos/"img$i.jpg"
done