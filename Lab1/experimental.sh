#!/bin/bash

for (( i=0; $i<${#animals[@]}; ((i++)) ))
do
    echo "\t${animals[$i]}: ${!animals[$i]}"
done