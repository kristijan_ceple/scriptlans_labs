#!/bin/bash

dst="${!#}"       # Here we will save the files
# First check if it exists
if [[ ! -e ./"$dst" ]]
then
    mkdir -p ./"$dst"
    echo "Created directory $dst"
fi

# Got it
succCopied=0
for (( c=1; c<$#; c++ ))
do
    file="${!c}"
    # Let's check if it already exists, and if it's readable
    if [ ! \( -e "$file" -a -r "$file" \)  ]
    then
        echo "File $file either not readable, doesn't exist, or both!"
        exit -1
    elif [ -d "$file" ]
    then
        echo "File $file is a directory - skipping backing up this!"
        continue
    fi

    # It's alright - then copy
    cp "$file" ./"$dst"/
    (( succCopied++ ))
done

echo "Successfully copied $succCopied files!"