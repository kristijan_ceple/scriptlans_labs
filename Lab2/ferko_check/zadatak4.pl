use Time::Piece;
use Time::Seconds;

while(<>) {
    # Let's get Jmbag, name, surname, year, month, day, hour and minutes, as well as turn in time data
    #@res = m/(\d{6});(\w+);\2;([\d-]{10}) (\d{2}:\d{2}) .*;\3 \4:\d{2}/;
    
    #First chomp
    chomp;

    # it's a .csv file, split by ";"
    my @res = split ";", $_;
    #print "@res";
    my ($jmbag, $surname, $name, $start_data, $turn_in) = @res;

    # For starters, the begin and turn-in dates must be the same string-wise
    $_ = $start_data;
    my @start_parts = split;
    $_ = $turn_in;
    my @end_parts = split;


    # This prototype was discarded due to my discovery of the Perl::Piece
    # my $problem = 0;
    # if($start_parts[0] eq $end_parts[0]) {
    #     # Need to check the times now!
    #     my $start_time_str = $start_parts[1];
    #     my $turn_in_time_str = $end_parts[1];

    #     # Have to form time objects here
    #     my $start_time = Time::Piece->strptime(
    #        $start_time_str,
    #        "" 
    #     );
    # } else {
    #     $problem = 1;
    # }

    my $start_datetime_str = $start_parts[0] . " " . $start_parts[1];
    my $turn_in_datetime_str = $turn_in;

    my $start_datetime = Time::Piece->strptime(
        $start_datetime_str,
        "%Y-%m-%d %H:%M"
    );

    my $turn_in_datetime = Time::Piece->strptime(
        $turn_in_datetime_str,
        "%Y-%m-%d %H:%M:%S"
    );

    # print "$name $surname :\n";
    # print "\tBegan: $start_datetime_str\n";
    # print "\tFinished: $turn_in_datetime_str\n";
    # print "\tBegan Time Piece: $start_datetime\n";
    # print "\tFinished Time Piece: $turn_in_datetime\n";

    my $delta = $turn_in_datetime - $start_datetime;
    if($delta > ONE_HOUR) {
        print "$jmbag $surname $name - PROBLEM: $start_datetime_str --> $turn_in_datetime_str\n";
    }
}