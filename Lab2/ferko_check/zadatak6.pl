#!/usr/bin/perl -w

use warnings;
use Scalar::Util qw(looks_like_number);
use open ':locale';
use locale;
# use utf8;
# binmode(STDOUT, "encoding(UTF-8)");

# Let us do the last one
# First check num of args
# print scalar @ARGV;
#print "\n";

if(@ARGV < 1) {
    die "At least 1 argument required - the prefixes length!\n";
}

$prefLen = pop @ARGV;
if (!looks_like_number($prefLen)) {
    die "Last argument should be numerical --> the prefix length!\n";
}

# Let's get all words first
while(<>) {
    #push @completeText, $_;
    push @words, (split m/[\s\,\;\.\"\(\)\'\!\?\:]+/, $_);
}

#print "@completeText";
#print "\n-------------------------------------\n";
#print "@words\n";

# Have got all the words now, as well as the complete text
# Now will go through each word!
foreach (@words) {
    if(length($_) >= $prefLen) {
        #print "$_\n";
        $pref = substr($_, 0, $prefLen);
        # print "Prefix is: $pref\n";
        # Have got the word. Now time to count the occurences!
        my @count = ("@words" =~ m~\b$pref.*?\b~gi);
        $word_counter{lc($pref)} = scalar @count;
        # print "$_ => " . scalar @count . "\n";
    }
}

# Sorted print! Oorah!
foreach (sort keys %word_counter) {
    printf "$_ : $word_counter{$_}\n";
}