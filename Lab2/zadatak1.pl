#!/usr/bin/perl -w

my ($line, $n);

print "Please, enter the string to be repeated >";
chomp($line = <STDIN>);

print "Please enter the amount of time to repeat >";
chomp($n = <STDIN>);

if(!($n =~ /^\d+\.?\d*$/g)) {
    die "$n must be a positive NUMBER of repetitions!";
}

foreach (1..$n) {
    print "$line\n";
}
