my %days;

%months = qw (
    jan 01
    feb 02
    mar 03
    apr 04
    may 05
    jun 06
    jul 07
    aug 08
    sep 09
    sept 09
    oct 10
    nov 11
    dec 12
);

while(<>) {
    # Skip empty only-whitespace lines
    if(/^\s*$/){
        next
    }
    
    # Which date are we reading? - extract from $ARGV or line

    # Now fill the table with values
    # Extract the hours!
    m{.*         # Anything at the beginning of the file
    \[(\d{2})/(\w+)/(\d{4})     # Extract the date in case reading from STDIN
    :(\d{2})      # Two digits(hours)
    :\d{2}      # Another 2 digits(mins)
    :\d{2} +        #And finally seconds
    .*          # Whatever,
    ".*"        # Followed by the http description(GET, POST,...)
    .*          # Some numbers after that
    }x;
    my $hour = $4;
    #print "$hour\n";

    # Time to extract the date
    my $date;
    if ($ARGV ne "-") {
        # Read from file name
        $ARGV =~ /localhost_access_log\.([\d-]+)\.txt/;
        $date = $1;
    } else {
        # Have to convert... oh Jesus have mercy upon me
        my ($year, $month, $day);
        $year = $3;
        $day = $1;
        $month = $months{lc $2};        # Lower-case the month name in order to universally get the number from the hash
        $date = join "-", ($year, $month, $day);
    }

    $days{$date}{$hour} += 1;
}

$, = " ";
# Reading done, time to print everything
# while ( my ($currDate, $currHash) = each %days ) {
#     print "$currDate\n";
#     # Inside default var there is another hash :D
#     while( my ($key, $value) = each %{$currHash}) {
#         print "\t$key => $value\n";
#     }
# }

foreach my $date (sort keys %days) {
    print " Datum: $date\n";
    print " sat : broj_pristupa\n";
    print "-------------------------------\n";

    foreach my $hour (sort keys %{$days{$date}}) {
        print "  $hour : $days{$date}{$hour}\n";
    }
    print "\n";
}