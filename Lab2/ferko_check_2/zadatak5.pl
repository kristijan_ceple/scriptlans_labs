#!/usr/bin/perl -w

if(@ARGV != 1) {
    die "Exactly one file should be passed as argument!\n";
}

open FROM, "<", $ARGV[0];

chomp(my $factors_str = <FROM>);
my @factors = split ";", $factors_str;

#print "@factors\n";
# Will calculate, and put results into the students hash
while(<FROM>) {
    chomp;
    my @student_res = split ";", $_;
    my $jmbag = shift @student_res;
    my $surname = shift @student_res;
    my $name = shift @student_res;
    my $sum = 0;
    while(1) {
        my $points = shift @student_res;
        if(!defined($points)) {
            last;
        } elsif ($points eq '-') {
            $points = 0;
        }
        
        my $coef = shift @factors;  # Using the factors as a circular list opp
        push @factors, $coef;
        $sum += $coef * $points;
    }

    my %student = (
        "name" => $name,
        "surname" => $surname,
        "score" => $sum
    );

    $students{$jmbag} = \%student;
}

# Let's first print the hash to see if everything is correct
# foreach $jmbag (keys %students) {
#     print "$jmbag:\n";
#     while ( ($key, $value) = each %{$students{$jmbag}}) {
#         print "\t$key => $value\n";
#     }
# }
# Everything stored nicely

# Let's print out things
print "Lista po rangu:\n";
print "-------------------\n";

$counter = 1;
foreach $jmbag (sort score_sort keys %students) {
    my %student = %{$students{$jmbag}};
    my $name = $student{"name"};
    my $surname = $student{"surname"};
    my $score = $student{"score"};
    my $roundedScore = sprintf("%.2f", $score);
    #print "  $counter. $surname, $name ($jmbag)\t\t: $score\n";
    printf "%-40s%s\n",
        "  $counter. $surname, $name ($jmbag)", ": " . $roundedScore;
    $counter++;
}

sub score_sort {
    # my ($first, $second) = @_;

    %firstStudent = %{$students{$a}};
    %secondStudent = %{$students{$b}};

    $firstScore = $firstStudent{"score"};
    $secondScore = $secondStudent{"score"};

    if($firstScore < $secondScore) {
        return 1;
    } elsif($firstScore == $secondScore) {
        return 0;
    } else {
        return -1;
    }
}