my @nums;
while(<>){
    chomp;
    my @line_nums = m/(-?\d+\.?\d*){1,}/g;
    if(!@line_nums) {
        die "Not a number entered!";
    }

    push @nums, @line_nums;
}

print "\nEntered:\n";
print "@nums\n";

# Calculate the mean
my $sum = 0;
foreach (@nums) {
    $sum += $_;
}

$sum /= ($#nums + 1);
print "The mean is: $sum\n";