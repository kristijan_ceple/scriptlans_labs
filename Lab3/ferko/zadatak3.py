import os
import re
import collections
import argparse

students = {}
# labs = {}
LabData = collections.namedtuple('LabData', ('points', 'group'))

def load_students(file_path: str):
    with open(file_path, 'r') as file:
        for line in file:
            (jmbag, surname, name) = line.rstrip().split(' ')
            students[jmbag] = {
                'name': name,
                'surname': surname,
                'jmbag': jmbag,
                'labs': {}
            }

def load_lab_data(file_path: str, lab: int, group: int):
    with open(file_path, 'r') as file:
        for line in file:
            # Each line contains jmbag and points
            (jmbag, points) = line.rstrip().split(' ')
            student_lab = students[jmbag]['labs'].get(lab)
            if student_lab is not None:
                # For same lab student occurred in multiple groups
                raise RuntimeWarning("Student found in multiple groups in the same lab!")
            else:
                # Add the points for the lab
                students[jmbag]['labs'][lab] = LabData(points=float(points), group=group)

def handle_data(files_dir: str):
    files = os.listdir(files_dir)
    # Need to find the students file
    if 'studenti.txt' not in files:
        raise FileNotFoundError("Students file not found!")

    files.remove('studenti.txt')
    load_students(files_dir + '/studenti.txt')

    # Students loaded - move on to load the labs data inside dicts/sets gonna see which I'm gonna use
    files = os.listdir(files_dir)

    # Set up regex for format Lab03_g08.txt
    p = re.compile(r'^Lab(\d+)_g(\d+)\.txt$')
    # labs_files = [(file, 2) for file in files if (mtch = p.match(file))]          # Got the lab files

    for file in files:
        mtch = p.match(file)

        if mtch is not None:
            # Found a lab file!
            lab = mtch.group(1)
            group = mtch.group(2)
            load_lab_data(files_dir + '/' + file, lab, group)

    # All loaded

def pretty_print():
    # Print header first
    header = '{:<15s}{:<30s}'.format('JMBAG', 'Prezime, Ime')
    # Have to count the labs now -> pick a random student and count how many labs are there
    stud = students.popitem()           # Pop the item in order to count the labs
    labs_count = len(stud[1]['labs'])
    students[stud[0]] = stud[1]         # Add the item back

    format_str = '{:<10s}' * labs_count
    labs_list = ['L{}'.format(i) for i in range(1, labs_count + 1)]
    input_fmt_str = "'" + "', '".join(labs_list) + "'"
    command = "header += '" + format_str + "'.format(" + input_fmt_str + ")\n"
    command += "print(header)"
    exec(command)

    # Move on to students themselves
    # students_iter = students.values()       # wanna sort this by jmbags alphabetically
    # students_iter.sort
    for student in sorted(students.values(), key=lambda item: item['jmbag']):
        to_print = '{:<15s}{:<30s}'.format(student['jmbag'], student['surname'] + ', ' + student['name'])
        for lab in student['labs']:
            to_print += '{:<10.1f}'.format(student['labs'][lab].points)
        print(to_print)


# Set up args
parser = argparse.ArgumentParser()
parser.add_argument("dir", type=str,
                    help="Name of directory where the student and lab files are saved to. ")
args = parser.parse_args()

handle_data(args.dir)
# print(students)
pretty_print()
