import argparse
import re

# Let's set up arg parse
parser = argparse.ArgumentParser()
parser.add_argument("web_page", type=str,
                    help="Web page that will be searched for in the log file for accesses.")
parser.add_argument("log_file", type=str,
                    help="Log file that contains the logged accesses.")
args = parser.parse_args()

# First try to open the log file
res = {}            # Prep the dict
with open(args.log_file, 'r') as read_file:
    if not read_file.readable():
        raise IOError('The log file is not readable! Exiting!')

    # Prepare the regex!
    regex_pages = re.compile(r'(\d{1,3})\.(\d{1,3})\.\d{1,3}\.\d{1,3}.*?"\w+? .*' + args.web_page + r'.+?" .+', re.IGNORECASE)
    for line in read_file:
        regex_results = regex_pages.findall(line)
        #print(regex_results)           # Control

        if len(regex_results) == 0:
            continue

        first_num = regex_results[0][0]
        second_num = regex_results[0][1]
        key = (first_num, second_num)

        # print(' '.join((first_num, second_num)))
        # print('Test')

        # Now, add them to the dictionary
        if res.get(key) is None:
            res[key] = 1
        else:
            res[key] += 1

# Fancy, now just print it sorted!
print(f"""-------------------------------------------
Broj pristupa stranici: {args.web_page}
  IP podmreza : Broj pristupa
-------------------------------------------""")

descending_items = sorted(res.items(), key=lambda t: t[1], reverse=True)
for item in descending_items:
    # If less than 2 times - skip!
    if item[1] < 2:
        continue
        #pass

    print('.'.join(item[0]) + '.*.* : ' + str(item[1]))

print("-------------------------------------------")
