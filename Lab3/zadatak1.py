import argparse
from collections import namedtuple
from typing import NamedTuple, Dict

mat_dim_nt = namedtuple('MatrixDimensions', ('mat_name', 'rows', 'cols'))

def complete_mat_str(to_print_mat_data: NamedTuple) -> str:
    mat: Dict = to_print_mat_data['mat']
    mat_dimms = to_print_mat_data['mat_dimms']

    to_ret = str(mat_dimms.rows) + ' ' + str(mat_dimms.cols) + '\n'
    for i in range(1, mat_dimms.rows + 1):
        for j in range(1, mat_dimms.cols + 1):
            # to_ret += str(round(mat.get((i, j), 0), 2)) + '  '
            to_ret += '{:6.2f}  '.format(mat.get((i, j), 0))

        to_ret += '\n'

    return to_ret


def compact_mat_str(to_print_mat_data: NamedTuple) -> str:
    mat: Dict = to_print_mat_data['mat']
    mat_dimms = to_print_mat_data['mat_dimms']

    to_ret = str(mat_dimms.rows) + ' ' + str(mat_dimms.cols) + '\n'
    for ((row, col), val) in mat.items():
        to_ret += str(row) + ' ' + str(col) + ' ' + str(val) + '\n'

    return to_ret


def load_matrix_data(file_path: str):
    file = open(file_path, "r")

    ##########################################       FIRST MAT           ###############################################
    mat_1 = {}
    mat_1_dim = None

    line = file.readline().rstrip()
    mat_1_dim_list = line.split(' ')              # Need to turn this into a tuple
    mat_1_dim = mat_dim_nt('mat1', int(mat_1_dim_list[0]), int(mat_1_dim_list[1]))

    # Time to read concrete matrix data!
    line = file.readline().rstrip()
    while line != '':
        line = line.rstrip()
        (row, col, val) = line.split(' ')
        # Need to convert them to numerical types first
        row = int(row)
        col = int(col)
        val = float(val)

        # Need to check row and col
        if row > mat_1_dim.rows or col > mat_1_dim.cols:
            raise IndexError('Invalid dimensions!')

        # mat_1[row][col] = val     --> doesn't work
        # mat_1.setdefault(row, {col: val})[col] = val
        mat_1[row, col] = val
        line = file.readline().rstrip()
    # First matrix loaded!
    ##########################################       FIRST MAT           ###############################################
    
    # Time for second matrix
    ##########################################       SECOND MAT           ##############################################
    mat_2 = {}
    mat_2_dim = None

    line = file.readline().rstrip()
    mat_2_dim_list = line.split(' ')  # Need to turn this into a tuple
    mat_2_dim = mat_dim_nt('mat2', int(mat_2_dim_list[0]), int(mat_2_dim_list[1]))

    # Need to check dimensions
    if mat_1_dim.cols != mat_2_dim.rows:
        raise ValueError('Dimension mismatch! Cannot multiply!')

    # Time to read concrete matrix data!
    line = file.readline().rstrip()
    while line != '':
        line = line.rstrip()
        (row, col, val) = line.split(' ')
        # Need to convert them to numerical types first
        row = int(row)
        col = int(col)
        val = float(val)

        # Need to check row and col
        if row > mat_2_dim.rows or col > mat_2_dim.cols:
            raise IndexError('Invalid dimensions!')

        # mat_2.setdefault(row, {col: val})[col] = val
        mat_2[row, col] = val
        line = file.readline().rstrip()
    # Second matrix loaded!
    ##########################################       SECOND MAT           ##############################################

    # We're done -- return the matrices and their dimms
    file.close()
    return {
        'mat1': {
            'mat': mat_1,
            'mat_dimms': mat_1_dim
        },
        'mat2': {
            'mat': mat_2,
            'mat_dimms': mat_2_dim
        }
    }


def mul_mats(matrices_to_mul):
    mat1_data = matrices_to_mul['mat1']
    mat2_data = matrices_to_mul['mat2']

    mat1 = mat1_data['mat']
    mat1_dimms = mat1_data['mat_dimms']

    mat2 = mat2_data['mat']
    mat2_dimms = mat2_data['mat_dimms']

    res_mat = {}
    res_mat_rows = mat1_dimms.rows
    res_mat_cols = mat2_dimms.cols
    res_mat_dimms = mat_dim_nt('res_mat', res_mat_rows, res_mat_cols)

    for i in range(1, res_mat_rows + 1):
        # Form(find out) row of mat1, and col of mat2 to mul
        row_2_mul = []
        for ((row1, col1), val1) in mat1.items():
            if row1 != i:
                continue
            else:
                row_2_mul.append((col1, val1))

        for j in range(1, res_mat_cols + 1):
            # Sum of products of i-th row and j-th col
            sum = 0
            col_2_mul = []

            for ((row2, col2), val2) in mat2.items():
                if col2 != j:
                    continue
                else:
                    col_2_mul.append((row2, val2))

            # Got all the values needed, time to summ and add to the matrix!
            for (index1, val1) in row_2_mul:
                for (index2, val2) in col_2_mul:
                    # Check indices first
                    if index1 == index2:
                        sum += val1 * val2
                        break

            # Just add the sum to the res matrix, and continue on
            if sum != 0:
                res_mat[i, j] = sum

    # All done, return the res mat
    return {
        'mat': res_mat,
        'mat_dimms': res_mat_dimms
    }


# Let's set up arg parse
parser = argparse.ArgumentParser()
parser.add_argument("input_file", type=str,
                    help="Input file containing matrices that will be multiplied")
parser.add_argument("output_file", type=str,
                    help="Output file where the multiplication result matrix will be written to")
args = parser.parse_args()

mats_input_path = args.input_file
matrices_to_mul = load_matrix_data(mats_input_path)

#print(matrices_to_mul)
res_mat = mul_mats(matrices_to_mul)
#print(res_mat)

# Let's write res_mat to file
mat_output_file = args.output_file
to_write_compact = compact_mat_str(res_mat)
with open(mat_output_file, 'w') as output_f:
    output_f.write(to_write_compact)

complete_str = complete_mat_str(res_mat)
print(complete_str)
