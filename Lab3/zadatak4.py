import argparse
import re
import urllib.request

# Set up args
parser = argparse.ArgumentParser()
parser.add_argument("url", type=str,
                    help="Web page that will be operated on.")
args = parser.parse_args()

print("#####################################        WEB PAGE        ##################################################")
web_page_obj = urllib.request.urlopen(args.url)
web_page_bytes = web_page_obj.read()
web_page = web_page_bytes.decode("utf8")
print(web_page)
print("#####################################        WEB PAGE        ##################################################")

print("#######################################      LINKS       ######################################################")
regex_links = re.compile(r'href\s*?=\s*?"(.+?)"', re.DOTALL | re.IGNORECASE)
found_links = regex_links.findall(web_page)
for item in found_links:
    print(item)
# print(found_links)
print("#######################################      LINKS       ######################################################")

print("#######################################      HOSTS       ######################################################")
# Okay, let's make a set of all the hosts
hosts = {}
regex_hosts = re.compile(r'.+?://(.+?)/.*?', re.DOTALL)
for item in found_links:
    mtch_obj = regex_hosts.match(item)
    if mtch_obj is not None:
        host = mtch_obj.group(1)
        val = hosts.get(host, 0)
        val += 1
        hosts[host] = val

for (key, val) in sorted(hosts.items(), key=lambda it: it[1], reverse=True):
    print(f"{key} appeared {val} times")
print("#######################################      HOSTS       ######################################################")

print("#######################################      EMAILS       #####################################################")
# Time to find emails
email_regex = re.compile(r'[\w\-\.]+?@[\w\-]+?\.[\w]+?')
emails = email_regex.findall(web_page)
for email in emails:
    print(email)
print("#######################################      EMAILS       #####################################################")

print("#######################################      PICS       #######################################################")
pics_regex = re.compile(r'<img\s+?src\s*?=\s*?".+?".*?>', re.DOTALL | re.IGNORECASE)
found_pics = pics_regex.findall(web_page)
for pic in found_pics:
    print(pic)
print('Pictures found quantity: ' + str(len(found_pics)))
print("#######################################      PICS       #######################################################")
