import argparse
from typing import List

QUANTILE_STEP = 10
QUANTILE_STEP_CPR = QUANTILE_STEP / 100


def process_hausdorff(hypotheses: List):
    # First have to sort them
    for hypothesis in hypotheses:
        hypothesis.sort()

    # Now we will make a list of quantiles for each hypothesis
    quantiles_res = []
    for i in range(len(hypotheses)):
        hypothesis = hypotheses[i]
        quantiles_res.append([])
        for curr_quant in range(10, 91, QUANTILE_STEP):
            n = len(hypothesis)
            element = hypothesis[int(curr_quant/100 * n) - 1]
            quantiles_res[i].append(element)

    return quantiles_res


def pretty_hausdorff(quantiles_list: List):
    to_ret_str = "Hyp"

    # First the header
    # percent_quantile_step = quantile_step * 100
    for i in range(10, 91, QUANTILE_STEP):
        to_ret_str += "#Q" + str(i)

    to_ret_str += '\n'
    # Got the header - move on
    for i in range(len(quantiles_list)):
        quantile_list = quantiles_list[i]
        to_append = '#'.join(map(str, quantile_list))
        to_ret_str += "{:03d}#".format(i + 1) + to_append + '\n'

    return to_ret_str

def load_data(file_path: str):
    file = open(file_path, 'r')

    if file.readable() is False:
        raise IOError("Can't read the file!")

    hypotheses = [list(map(float, line.rstrip().split(' '))) for line in file]
    return hypotheses


# Let's set up arg parse
parser = argparse.ArgumentParser()
parser.add_argument("input_file", type=str,
                    help="Input file containing matrices that will be multiplied")
args = parser.parse_args()

hypotheses = load_data(args.input_file)

# Time to process them
quantiles_res = process_hausdorff(hypotheses)
output_str = pretty_hausdorff(quantiles_res)
print(output_str)
